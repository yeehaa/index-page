(ns landing-page.template
  (:require [landing-page.head :refer [Head]]
            [ui-components.logo :refer [Logo]]
            [ui-components.menubar :refer [Menubar]]))

(defn template [page-content]
  [:html
   [Head]
   [:body
    [:noscript
     [:iframe {:src "https://www.googletagmanager.com/ns.html?id=GTM-K69GHMD"
               :height "0"
               :width "0"
               :style {:display "none"
                       :visibility "hidden"}}]]
    [:div#app
     [:ui.grid
      [Menubar {:logo [Logo "Offcourse_"]}]
      [:script {:type "text/javascript" :src "/offcourse.js"}]]]]])

