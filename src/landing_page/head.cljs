(ns landing-page.head)

(defn Head []
  [:head
   [:meta {:charset "utf-8"}]
   [:title "Offcourse"]
   [:link {:rel "stylesheet" :href "/css/style.css"}]
   [:link {:rel "stylesheet" :href "/css/overrides.css"}]
   [:link {:rel "icon" :href "/images/favicon.ico?v=2"}]
   [:script {:src "/analytics.js"}]
   [:meta {:name    "viewport"
           :content "width=device-width, initial-scale=1.0"}]])
