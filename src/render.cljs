(ns landing-page.render
  (:require [cljs.nodejs :as node]
            [landing-page.template :refer [template]]
            [cljsjs.js-yaml :as yaml]
            [reagent.core :as r]
            [reagent.dom.server :as rs]))

(def fs (node/require "fs"))

(defn ^:export render-page []
  (let [file-content (.readFileSync fs "./content/landing-page.yml" "utf-8")
        content (js->clj (.safeLoad js/jsyaml file-content) :keywordize-keys true)]
    (rs/render-to-static-markup [template content])))

(.log js/console (render-page))
